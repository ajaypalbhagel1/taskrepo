const express = require('express');
const router = express.Router();
const userService = require('../services/userService')

const jwt = require('jsonwebtoken');
const passport = require('passport');
const tokenExpireTime = { expiresIn: "30m" }
const tokenSecretKey = 'secret!@#$!@!@'


// user sign up controller
router.post('/signup', async (req, res, next) => {
    const [error, errorMsg, result] = await userService.userRegistration(req);
    if (error) {
        return res.status(500).json({
            message: errorMsg
        })
    } else {
        res.status(200).json({
            response: result
        })
    }
});

// user login controller
router.post('/login', async (req, res, next) => {
    passport.authenticate('local', { session: false }, (err, user, info) => {
        if (err || !user[0]) {
            return res.status(400).json({
                message: info ? info.message : 'Login failed',
                user: user
            });
        }

        req.login(user[0], { session: false }, (err) => {
            if (err) {
                res.send(err);
            }
            var user1 = user[0]
            const token = jwt.sign(user1, tokenSecretKey,
                tokenExpireTime)
                return res.json({ ...user1, token });
        });
    })(req, res);
});

module.exports = router
