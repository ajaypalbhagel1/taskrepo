const express = require('express');
const router = express.Router();
const taskService = require('../services/taskService')

// add task
router.post('/', async (req, res, next) => {
    const [error, errorMsg, result] = await taskService.insertTask(req);
    if (error) {
        return res.status(500).json({
            message: errorMsg
        })
    } else {
        res.status(200).json({
            response: result
        })
    }
});

// update task
router.put('/:taskId', async (req, res, next) => {
    const [error, errorMsg, result] = await taskService.updateTask(req);
    if (error) {
        return res.status(500).json({
            message: errorMsg
        })
    } else {
        res.status(200).json({
            response: result
        })
    }
});

// query task
router.get('/', async (req, res, next) => {
    const [error, errorMsg, result] = await taskService.selectTaskResult(req);
    if (error) {
        return res.status(500).json({
            message: errorMsg
        })
    } else {
        res.status(200).json({
            response: result
        })
    }
});

// delete task
router.delete('/:taskId', async (req, res, next) => {
    const [error, errorMsg, result] = await taskService.deleteTask(req);
    if (error) {
        return res.status(500).json({
            message: errorMsg
        })
    } else {
        res.status(200).json({
            response: result
        })
    }
});

module.exports = router