const passport = require('passport');
const passportJWT = require("passport-jwt");
const ExtractJWT = passportJWT.ExtractJwt;
const LocalStrategy = require('passport-local').Strategy;
const JWTStrategy = passportJWT.Strategy;
const util = require('../utils/dbWrapper')
var GoogleStrategy = require('passport-google-oauth20').Strategy;
const bcrypt = require('bcrypt');

passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password'
},
    function (email, password, cb) {
        var stringQueary = `select * from user where email= '${email}'`;
        return util.executeQuery(stringQueary)
            .then(user => {
                if (!user) {
                    return cb(null, false, { message: 'Incorrect email or password.' });
                }
                var comparePassword = bcrypt.compareSync(password, user[0].password);
                if (!comparePassword) {
                    return cb(null, false, { message: 'Incorrect email or password.' });
                }
                return cb(null, user, {
                    message: 'Logged In Successfully'
                });
            })
            .catch(err => {
                return cb(err);
            });
    }
));

passport.use(new JWTStrategy({
    jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
    secretOrKey: 'secret!@#$!@!@'
},
    function (jwtPayload, cb) {
        //find the user in db if needed
        var stringQueary = `select * from user where id= ${jwtPayload.id}`;
        return util.executeQuery(stringQueary)
            .then(user => {
                var user1 = user[0]
                return cb(null, user1);
            })
            .catch(err => {
                return cb(err);
            });
    }
));

passport.serializeUser(function (user, done) {
    done(null, user.id);
});

passport.deserializeUser(function (id, done) {
    var stringQueary = `select * from user where id= ${id}`;
    util.executeQuery(stringQueary)(function (err, user) {
        done(err, user);
    });
    done(null, id)
});


passport.use(new GoogleStrategy({
    clientID: "557773497474-achf9jgd04saqgdg5lco4v29i7sjv0kd.apps.googleusercontent.com",
    clientSecret: "ecZJbwStivvsHs_38qGOvSuO",
    callbackURL: "http://localhost:3000/google/callback"
},
    function (accessToken, refreshToken, profile, cb) {
        findOrCreate(profile)
        return cb(null, profile)
    }
));

async function findOrCreate(profile) {
    var stringQueary = `select count(*) as count from user where email= '${profile._json.email}'`;
    var user = await util.executeQuery(stringQueary)
    var insertQuery = ''
    if (user[0].count == 0) {
        insertQuery = `INSERT INTO user (name, email, password, role_id) VALUES ('${profile._json.name}','${profile._json.email}','null',2)`;
        var user1 = await util.executeQuery(insertQuery)
    }
}