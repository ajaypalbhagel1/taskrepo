const util = require('../utils/dbWrapper')

// add task api service
async function insertTask(req) {
    var request = req.body;
    let error = false
    var responseData = ''
    let errorMsg = ''
    var stringQueary = `INSERT INTO task (title, user_id) VALUES ('${request.title}',${request.user_id})`;
    responseData = await util.executeQuery(stringQueary)

    return [error, errorMsg, responseData];
}

// update task api service
async function updateTask(req) {
    var request = req.body;
    var taskID = req.params.taskId
    let error = false
    var responseData = ''
    let errorMsg = ''
    var stringQueary = `UPDATE task
    SET title = '${request.title}'
    WHERE id = ${taskID}`;
    responseData = await util.executeQuery(stringQueary)


    return [error, errorMsg, responseData];
}

// delete task api service
async function deleteTask(req) {
    var request = req.body;
    var taskID = req.params.taskId
    let error = false
    var responseData = ''
    let errorMsg = ''
    var stringQueary = `DELETE FROM task WHERE id = ${taskID}`;
    responseData = await util.executeQuery(stringQueary)

    return [error, errorMsg, responseData];
}

// select task api service
async function selectTaskResult(req) {
    var request = req.body;
    var taskID = req.params.taskId
    let error = false
    var responseData = ''
    var stringQueary = ''
    let errorMsg = ''
    if(true){
     stringQueary = 'select * from task';
    }else{
     stringQueary = 'select * from task where user_id=${user_id}';
    }
    responseData = await util.executeQuery(stringQueary)
    return [error, errorMsg, responseData];
}


module.exports = { selectTaskResult, updateTask, insertTask, deleteTask }