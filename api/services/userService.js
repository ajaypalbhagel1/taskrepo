const bcrypt = require('bcrypt');
const util = require('../utils/dbWrapper')

// user signup api service

async function userRegistration(req) {
    var request = req.body;
    let error = false
    var responseData = ''
    let errorMsg = ''
    // converting hash format password
    let hash = await bcrypt.hash(request.password, 10)
    // checking email already exits or not
    var stringQueary = `INSERT INTO user (name, email, password, role_id) VALUES ('${request.name}','${request.email}','${hash}',${request.role_id})`;
    responseData = await util.executeQuery(stringQueary)
    return [error, errorMsg, responseData];
}

module.exports = { userRegistration }