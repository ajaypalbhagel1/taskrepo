var MySql = require('sync-mysql');
var connection = new MySql({
  host: 'localhost',
  user: 'root',
  password: 'root',
  database: 'tasks'
});
async function executeQuery(queryString) {
  console.log('dbQuery ', queryString)
  const result = await connection.query(queryString);
  console.log(result)
  return (result)
}



module.exports = { executeQuery }