var express = require('express');
var bodyParser = require('body-parser');
const taskRoutes = require('./api/controller/taskController')
const userRoutes = require('./api/controller/userController')
var app = express();
const passport = require('passport');
require('./api/middleware/passport');
var cookieSession = require('cookie-session')
const tokenExpireTime = { expiresIn: "30m" }
const tokenSecretKey = 'secret!@#$!@!@'
const jwt = require('jsonwebtoken');

app.use(passport.initialize());
app.use(passport.session());

app.use('/uploads', express.static('uploads'))
app.use(cookieSession({
    name: 'task-session',
    keys: ['key1', 'key2']
}))

//use body parser for parsing req body
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//enabling cors
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Accept, Content-Type, Authorization');
    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'PUT, POST , PATCH , DELETE')
        return res.status(200).json({
        })
    }

    next();
});

app.use('/task', passport.authenticate('jwt', { session: false }), taskRoutes)
app.use('/user', userRoutes)

app.get('/failed', (req, res) => res.send('failed to login..'))
app.get('/', (req, res) => res.send('you are not log in..'))
app.get('/logout', (req, res) => {
    req.session = null;
    req.logOut;
    res.redirect('/')
})

app.get('/google',
    passport.authenticate('google', { scope: ['profile', 'email'] }));

app.get('/google/callback',
    passport.authenticate('google', { failureRedirect: '/failed' }),
    async function (req, res) {
        // Successful authentication, redirect home.
        const token = jwt.sign(req.user._json, tokenSecretKey,
            tokenExpireTime)
        return res.json({ ...req.user._json, token });
    });

app.use((req, res, next) => {
    const error = new Error("Not Found");
    error.status = 404
    next(error)
})

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error:
        {
            message: error.message
        }
    });
});


module.exports = app;